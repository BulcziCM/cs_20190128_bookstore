﻿using BookStore.Business.Models;
using System;
using System.Collections.Generic;

namespace BookStore.Cli.Helpers
{
    internal class IoHelper
    {
        public void PrintBooks(IEnumerable<BookBl> books)
        {
            foreach (var book in books)
            {
                Console.WriteLine($"{book.Id}: {book.Title} by {book.Author.Name} {book.Author.Surname}");
            }
        }

        public void PrintAuthors(IEnumerable<AuthorBl> authors)
        {
            foreach (var author in authors)
            {
                Console.WriteLine($"{author.Id}: {author.Name} {author.Surname}");
            }
        }

        public void PrintBookStores(IEnumerable<BookStoreBranchBl> bookStores)
        {
            foreach (var bookStore in bookStores)
            {
                Console.WriteLine($"{bookStore.Id}: {bookStore.Name} in city of {bookStore.City}");
            }
        }
    }
}