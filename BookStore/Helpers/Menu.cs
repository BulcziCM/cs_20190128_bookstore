﻿using System;
using System.Collections.Generic;

namespace BookStore.Cli.Helpers
{
    internal class Menu
    {
        //public delegate void MenuCommandHandle();
        private Dictionary<string, Action> _commands = new Dictionary<string, Action>();

        public void SetCommand(Action command, string commandId)
        {
            if(_commands.ContainsKey(commandId))
            {
                throw new Exception($"Command with id {commandId} already exists!");
            }

            _commands.Add(commandId, command);
        }

        public void Execute(string commandId)
        {
            if (!_commands.ContainsKey(commandId))
            {
                throw new Exception($"There is no command with id {commandId}!");
            }

            var chosenCommand = _commands[commandId];
            chosenCommand();
        }

        public void PrintAllCommands()
        {
            Console.WriteLine("Available commands:");
            foreach(var item in _commands)
            {
                Console.WriteLine($"- {item.Key}");
            }
        }
    }
}