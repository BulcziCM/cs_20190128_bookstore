﻿using BookStore.Cli.Helpers;
using BookStore.Business;
using System;
using System.Linq;
using BookStore.Business.Models;
using System.Globalization;
using System.Collections.Generic;

namespace BookStore.Cli
{
    internal class Program
    {
        private Menu _menu = new Menu();
        private IoHelper _ioHelper = new IoHelper();

        private AuthorsService _authorsService;
        private BooksService _bookService;
        private BookStoresService _bookStoresService;
        private SubscriptionsService _subscriptionsService;
        private IncomeReportGenerator _incomeReportGenerator;

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to BookStore");

            var program = new Program();
            program.SetCommands();
            program.ProgramLoop();

            Console.ReadLine();
        }

        public Program()
        {
            _bookService = new BooksService();
            _bookStoresService = new BookStoresService();
            _authorsService = new AuthorsService();
            _incomeReportGenerator = new IncomeReportGenerator(_bookService, _bookStoresService);

            _subscriptionsService = new SubscriptionsService(_bookService);
        }

        private void SetCommands()
        {
            _menu.SetCommand(AddAuthor, "AddAuthor");
            _menu.SetCommand(AddBook, "AddBook");
            _menu.SetCommand(AddBookStore, "AddBookStore");
            _menu.SetCommand(AddBookToBookStoreAsync, "AddBookToBookStore");
            _menu.SetCommand(AddSubscription, "AddSubscription");

            _menu.SetCommand(SellBook, "SellBook");
            _menu.SetCommand(ShowIncomePerBookStore, "ShowIncomePerBookStore");
            _menu.SetCommand(ShowIncomePerBook, "ShowIncomePerBook");

            _menu.SetCommand(PrintAllBooksAsync, "PrintAllBooks");
            _menu.SetCommand(GetGenreBooksPricesData, "GetGenreBooksPricesData");
            _menu.SetCommand(PrintAllAuthors, "PrintAllAuthors");
            _menu.SetCommand(PrintBooksDividedByGenreAsync, "PrintBooksDividedByGenre");
            _menu.SetCommand(SearchAsync, "Search");
        }

        public void ProgramLoop()
        {
            do
            {
                _menu.PrintAllCommands();
                _menu.Execute(Console.ReadLine());
            }
            while (true);
        }

        private void AddAuthor()
        {
            Console.WriteLine("Enter name:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter surname");
            var surname = Console.ReadLine();
            Console.WriteLine("Enter date (MM/DD/YYYY)");
            var dateOfBirth = DateTime.ParseExact(Console.ReadLine(), "d", CultureInfo.InvariantCulture);

            var author = new AuthorBl
            {
                Name = name,
                Surname = surname,
                DateOfBirth = dateOfBirth
            };

            _authorsService.AddAuthor(author);
        }

        private void AddBook()
        {
            var book = new BookBl();

            Console.WriteLine("Type the title of the book:");
            book.Title = Console.ReadLine();

            Console.WriteLine("Type surname of the author of the book:");
            book.Author = ChooseAuthorBySurname();

            Console.WriteLine("Type the genre of the book:");
            book.BookGenre = (BookBl.Genre)Enum.Parse(typeof(BookBl.Genre), Console.ReadLine());

            _bookService.AddAsync(book);
        }

        private void AddBookStore()
        {
            Console.WriteLine("Type city name:");
            var city = Console.ReadLine();
            Console.WriteLine("Type name");
            var name = Console.ReadLine();

            var bookStore = new BookStoreBranchBl()
            {
                City = city,
                Name = name,
                AvailableBooks = new List<BookInStockBl>()
            };

            _bookStoresService.AddBookStore(bookStore);
        }

        private async void AddBookToBookStoreAsync()
        {
            var books = await _bookService.GetAllAsync();
            _ioHelper.PrintBooks(books);

            Console.WriteLine("Choose book id:");
            var bookId = int.Parse(Console.ReadLine());

            var bookStores = _bookStoresService.GetAllBookStores();
            _ioHelper.PrintBookStores(bookStores);

            Console.WriteLine("Choose bookstore id:");
            var bookStoreId = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter book price:");
            var bookPrice = double.Parse(Console.ReadLine());

            var bookInStock = new BookInStockBl
            {
                BookId = bookId,
                BookStoreId = bookStoreId,
                Price = bookPrice
            };

            _bookStoresService.AddBookToBookStore(bookInStock);
        }

        private void SellBook()
        {
            Console.WriteLine("Provide book id: ");
            var bookId = int.Parse(Console.ReadLine());

            Console.WriteLine("Provide BookStore id: ");
            var bookStoreId = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter amount of books to by: ");
            var amout = int.Parse(Console.ReadLine());

            var priceToPay = _bookStoresService.SellBook(bookId, bookStoreId, amout);

            Console.WriteLine($"You need to pay ${priceToPay}");
        }

        private void ShowIncomePerBookStore()
        {
            var incomeReport = _incomeReportGenerator.GetIncomePerBookStore()
                .OrderByDescending(x => x.Value);

            foreach(var incomeRecord in incomeReport)
            {
                Console.WriteLine($"{incomeRecord.Key.Name} - ${incomeRecord.Value}");
            }
        }

        private void ShowIncomePerBook()
        {
            var incomeReport = _incomeReportGenerator.GetIncomePerBook()
                .OrderByDescending(x => x.Value);

            foreach (var incomeRecord in incomeReport)
            {
                Console.WriteLine($"{incomeRecord.Key.Title} - ${incomeRecord.Value}");
            }
        }

        private AuthorBl ChooseAuthorBySurname()
        {
            var authors = _authorsService.GetAllAuthors();
            _ioHelper.PrintAuthors(authors);

            var surname = Console.ReadLine();
            return authors.SingleOrDefault(a => a.Surname == surname);
        }

        private async void PrintAllBooksAsync()
        {
            var foundBooks = await _bookService.GetAllAsync();
            _ioHelper.PrintBooks(foundBooks);
        }

        private void AddSubscription()
        {
            Console.WriteLine("Enter your email: ");
            var email = Console.ReadLine();
            Console.WriteLine("Provide book id: ");
            var bookId = int.Parse(Console.ReadLine());

            var emailSubscription = new EmailSubscriptionBl();
            emailSubscription.Email = email;
            emailSubscription.BookId = bookId;

            _subscriptionsService.AddSubscription(emailSubscription);
        }

        private async void GetGenreBooksPricesData()
        {
            Console.WriteLine("Choose bookstore id:");
            var bookStoreId = int.Parse(Console.ReadLine());

            var booksPriceData = await _bookService.GetBooksPriceDataByGenreAsync(bookStoreId);

            foreach (var priceData in booksPriceData)
            {
                Console.WriteLine($"{priceData.Genre}: MinPrice: {priceData.MinPrice}; MaxPrice: {priceData.MaxPrice}; AvgPrice: {priceData.AvgPrice}");
            }
        }

        private async void PrintBooksDividedByGenreAsync()
        {
            foreach (var group in await _bookService.GetBooksGroupedByGenresAsync())
            {
                Console.WriteLine($"Genre: {group.Key}");

                _ioHelper.PrintBooks(group.Value);

                Console.WriteLine();
            }
        }

        private void PrintAllAuthors()
        {
            var authors = _authorsService.GetAllAuthors();

            foreach (var author in authors)
            {
                Console.WriteLine($"Author name: {author.Name} and surname: {author.Surname}");
            }
        }

        private async void SearchAsync()
        {
            Console.WriteLine("Type author name:");
            var namePart = Console.ReadLine();

            Console.WriteLine("Type title:");
            var titlePart = Console.ReadLine();

            var foundBooks = await _bookService.SearchAsync(namePart, titlePart);

            if (foundBooks.Any())
            {
                _ioHelper.PrintBooks(foundBooks);
            }
            else
            {
                Console.WriteLine($"No book found for criteria '{namePart}', '{titlePart}'");
            }
        }
    }
}