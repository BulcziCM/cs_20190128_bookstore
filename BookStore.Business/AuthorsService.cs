﻿using BookStore.Business.Helpers;
using BookStore.Business.Models;
using BookStore.Data;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.Business
{
    public class AuthorsService
    {
        private ModelsMapper _modelsMapper = new ModelsMapper();

        public IEnumerable<AuthorBl> GetAllAuthors()
        {
            IEnumerable<AuthorBl> results;

            using (var dbContext = new BookStoresDbContext())
            {
                results = dbContext.Authors
                    .ToList()
                    .Select(a => _modelsMapper.MapAuthorFromDataToBusiness(a));
            }

            return results;
        }

        public void AddAuthor(AuthorBl author)
        {
            var authorToAdd = _modelsMapper.MapAuthorFromBusinessToData(author);

            using (var dbContext = new BookStoresDbContext())
            {
                dbContext.Authors.Add(authorToAdd);
                dbContext.SaveChanges();
            }
        }
    }
}