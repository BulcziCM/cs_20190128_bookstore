﻿using System;
using System.Linq;
using BookStore.Business.Models;
using BookStore.Data.Models;

namespace BookStore.Business.Helpers
{
    internal class ModelsMapper
    {
        public BookBl MapBookFromDataToBusinessModel(Book book)
        {
            return new BookBl
            {
                Id = book.Id,
                Author = MapAuthorFromDataToBusiness(book.Author),
                BookGenre = MapBookGenreFromDataToBusiness(book.BookGenre),
                Title = book.Title
            };
        }

        public BookBl.Genre MapBookGenreFromDataToBusiness(Book.Genre bookGenre)
        {
            switch (bookGenre)
            {
                case Book.Genre.Comedy:
                    return BookBl.Genre.Comedy;
                case Book.Genre.Drama:
                    return BookBl.Genre.Drama;
                case Book.Genre.Horror:
                    return BookBl.Genre.Horror;
                case Book.Genre.Fantasy:
                    return BookBl.Genre.Fantasy;
                default:
                    throw new Exception($"Cannot map {bookGenre} to BookBl.Gener type");
            }
        }

        public Book MapBookFromBusinessToDataModel(BookBl bookBl)
        {
            return new Book
            {
                Id = bookBl.Id,
                Author = MapAuthorFromBusinessToData(bookBl.Author),
                BookGenre = MapBookGenreFromBusinessToData(bookBl.BookGenre),
                Title = bookBl.Title
            };
        }

        public Book.Genre MapBookGenreFromBusinessToData(BookBl.Genre bookGenre)
        {
            switch (bookGenre)
            {
                case BookBl.Genre.Comedy:
                    return Book.Genre.Comedy;
                case BookBl.Genre.Drama:
                    return Book.Genre.Drama;
                case BookBl.Genre.Horror:
                    return Book.Genre.Horror;
                case BookBl.Genre.Fantasy:
                    return Book.Genre.Fantasy;
                default:
                    throw new Exception($"Cannot map {bookGenre} to Book.Gener type");
            }
        }

        public EmailSubscription MapSubscriptionFromBusinessToData(EmailSubscriptionBl subscriptionBl)
        {
            return new EmailSubscription
            {
                Id = subscriptionBl.Id,
                BookId = subscriptionBl.BookId,
                Email = subscriptionBl.Email
            };
        }

        public EmailSubscriptionBl MapSubscriptionFromDataToBusiness(EmailSubscription subscription)
        {
            return new EmailSubscriptionBl
            {
                Id = subscription.Id,
                BookId = subscription.BookId,
                Email = subscription.Email
            };
        }

        public Author MapAuthorFromBusinessToData(AuthorBl authorBl)
        {
            return new Author
            {
                Id = authorBl.Id,
                Name = authorBl.Name,
                Surname = authorBl.Surname,
                DateOfBirth = authorBl.DateOfBirth
            };
        }

        public AuthorBl MapAuthorFromDataToBusiness(Author author)
        {
            if(author == null)
            {
                return null;
            }

            return new AuthorBl
            {
                Id = author.Id,
                Name = author.Name,
                Surname = author.Surname,
                DateOfBirth = author.DateOfBirth
            };
        }

        public BookStoreBranch MapBookStoreFromBusinessToDataModel(BookStoreBranchBl bookStoreBl)
        {
            return new BookStoreBranch
            {
                Id = bookStoreBl.Id,
                City = bookStoreBl.City,
                Name = bookStoreBl.Name,
                AvailableBooks = bookStoreBl.AvailableBooks?.Select(bookInStock => MapBookInStockFromBusinessToData(bookInStock)).ToList()
            };
        }

        public BookStoreBranchBl MapBookStoreFromDataToBusinessModel(BookStoreBranch bookStore)
        {
            return new BookStoreBranchBl
            {
                Id = bookStore.Id,
                City = bookStore.City,
                Name = bookStore.Name,
                AvailableBooks = bookStore.AvailableBooks?.Select(bookInStock => MapBookInStockFromDataToBusiness(bookInStock)).ToList()
            };
        }

        public BookInStock MapBookInStockFromBusinessToData(BookInStockBl bookInStockBl)
        {
            return new BookInStock
            {
                Id = bookInStockBl.Id,
                BookId = bookInStockBl.BookId,
                BookStoreBranchId = bookInStockBl.BookStoreId,
                Price = bookInStockBl.Price,
                SoldCopies = bookInStockBl.SoldCopies
            };
        }

        public BookInStockBl MapBookInStockFromDataToBusiness(BookInStock bookInStock)
        {
            return new BookInStockBl
            {
                Id = bookInStock.Id,
                BookId = bookInStock.BookId,
                BookStoreId = bookInStock.BookStoreBranchId,
                Price = bookInStock.Price,
                SoldCopies = bookInStock.SoldCopies
            };
        }
    }
}