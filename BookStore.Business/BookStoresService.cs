﻿using BookStore.Business.Helpers;
using BookStore.Business.Models;
using BookStore.Data;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.Business
{
    public interface IBookStoresService
    {
        void AddBookStore(BookStoreBranchBl bookStore);
        void AddBookToBookStore(BookInStockBl bookInStockBl);
        IEnumerable<BookStoreBranchBl> GetAllBookStores();
        Dictionary<BookStoreBranchBl, IEnumerable<BookInStockBl>> GetBookStoresStock();
        double SellBook(int bookId, int bookStoreId, int amount);
    }

    public class BookStoresService : IBookStoresService
    {
        private ModelsMapper _modelsMapper = new ModelsMapper();

        public void AddBookStore(BookStoreBranchBl bookStore)
        {
            var bookStoreToAdd = _modelsMapper.MapBookStoreFromBusinessToDataModel(bookStore);

            using (var dbContext = new BookStoresDbContext())
            {
                dbContext.BookStores.Add(bookStoreToAdd);
                dbContext.SaveChanges();
            }
        }

        public Dictionary<BookStoreBranchBl, IEnumerable<BookInStockBl>> GetBookStoresStock()
        {
            using (var dbContext = new BookStoresDbContext())
            {
                return dbContext.BooksInStock
                    .GroupBy(
                        bis => dbContext.BookStores.FirstOrDefault(bs => bs.Id == bis.BookStoreBranchId),
                        bis => bis)
                    .ToDictionary(
                        group => _modelsMapper.MapBookStoreFromDataToBusinessModel(group.Key),
                        group => group.Select(bis => _modelsMapper.MapBookInStockFromDataToBusiness(bis))
                    );
            }
        }

        public IEnumerable<BookStoreBranchBl> GetAllBookStores()
        {
            using (var dbContext = new BookStoresDbContext())
            {
                return dbContext.BookStores
                    .ToList()
                    .Select(bs => _modelsMapper.MapBookStoreFromDataToBusinessModel(bs));
            }
        }

        public void AddBookToBookStore(BookInStockBl bookInStockBl)
        {
            var bookInStock = _modelsMapper.MapBookInStockFromBusinessToData(bookInStockBl);

            using (var dbContext = new BookStoresDbContext())
            {
                dbContext.BooksInStock.Add(bookInStock);
                dbContext.SaveChanges();
            }
        }

        public double SellBook(int bookId, int bookStoreId, int amount)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                var bookInStock = dbContext.BooksInStock.
                    SingleOrDefault(bis => bis.BookId == bookId
                        && bis.BookStoreBranchId == bookStoreId
                    );

                if (bookInStock == null)
                {
                    //TODO Throw some cool exception
                    return 0.0d;
                }

                bookInStock.SoldCopies += amount;
                dbContext.SaveChanges();

                return amount * bookInStock.Price;
            }
        }
    }
}