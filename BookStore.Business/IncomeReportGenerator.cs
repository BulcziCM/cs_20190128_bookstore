﻿using BookStore.Business.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Business
{
    public class IncomeReportGenerator
    {
        private readonly IBooksService _booksService;
        private readonly IBookStoresService _bookStoresService;

        public IncomeReportGenerator(
            IBooksService booksService,
            IBookStoresService bookStoresService)
        {
            _booksService = booksService;
            _bookStoresService = bookStoresService;
        }

        public Dictionary<BookStoreBranchBl, double> GetIncomePerBookStore()
        {
            var bookStoresStock = _bookStoresService.GetBookStoresStock();

            var bookStoreIncome = bookStoresStock.ToDictionary(
                    group => group.Key,
                    group => group.Value.Sum(bis => bis.Price * bis.SoldCopies)
                );

            return bookStoreIncome;
        }

        public Dictionary<BookBl, double> GetIncomePerBook()
        {
            var bookStoresStock = _bookStoresService.GetBookStoresStock();

            var bookIncome = bookStoresStock
                .SelectMany(group => group.Value)
                .GroupBy(
                    bis => _booksService.GetAsync(bis.BookId).Result,
                    bis => bis.Price * bis.SoldCopies
                )
                .ToDictionary(
                    group => group.Key,
                    group => group.Sum(income => income)
                );

            return bookIncome;
        }
    }
}