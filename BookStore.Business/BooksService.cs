﻿using BookStore.Business.Helpers;
using BookStore.Business.Models;
using BookStore.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Business
{
    public interface IBooksService
    {
        void AddAsync(BookBl book);
        Task<BookBl> GetAsync(int bookId);
        Task<IEnumerable<BookBl>> GetAllAsync();
        Task<Dictionary<BookBl.Genre, IEnumerable<BookBl>>> GetBooksGroupedByGenresAsync();
        Task<IEnumerable<GenreBookPricesData>> GetBooksPriceDataByGenreAsync(int bookStoreId);
        Task<IEnumerable<BookBl>> SearchAsync(string namePart, string titlePart);
    }

    public class BooksService : IBooksService
    {
        private ModelsMapper _modelsMapper = new ModelsMapper();

        public EventHandler<BookAddedEventArgs> BookAdded;

        public async void AddAsync(BookBl book)
        {
            if(book.Author == null)
            {
                throw new ArgumentException("Cannot add a book with no Author");
            }

            if (string.IsNullOrWhiteSpace(book.Title))
            {
                throw new ArgumentException("Cannot add a book with no Title");
            }

            var bookToAdd = _modelsMapper.MapBookFromBusinessToDataModel(book);

            using (var dbContext = new BookStoresDbContext())
            {
                if (!dbContext.Authors.Local.Contains(bookToAdd.Author))
                {
                    dbContext.Authors.Attach(bookToAdd.Author);
                    dbContext.Entry(bookToAdd.Author).State = EntityState.Modified;
                }

                bookToAdd.Author = await dbContext.Authors.SingleOrDefaultAsync(a => a.Id == bookToAdd.Author.Id);
                var addedBook = dbContext.Books.Add(bookToAdd);
                await dbContext.SaveChangesAsync();

                book.Id = addedBook.Id;
            }

            OnBookAdded(book);
        }

        public async Task<BookBl> GetAsync(int bookId)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                var book = await dbContext.Books.SingleOrDefaultAsync(b => b.Id == bookId);
                return _modelsMapper.MapBookFromDataToBusinessModel(book);
            }
        }

        private void OnBookAdded(BookBl book)
        {
            if (BookAdded != null)
            {
                var eventArgs = new BookAddedEventArgs();
                eventArgs.ArrivedBook = book;

                BookAdded(this, eventArgs);
            }
        }

        public async Task<Dictionary<BookBl.Genre, IEnumerable<BookBl>>> GetBooksGroupedByGenresAsync()
        {
            Dictionary<BookBl.Genre, IEnumerable<BookBl>> result;

            using (var dbContext = new BookStoresDbContext())
            {
                result = (await dbContext.Books
                    .Include(b => b.Author)
                    .ToListAsync())
                    .GroupBy(book => book.BookGenre, book => book)
                    .ToDictionary(
                        group => _modelsMapper.MapBookGenreFromDataToBusiness(group.Key),
                        group => group.Select(book => _modelsMapper.MapBookFromDataToBusinessModel(book))
                    );
            }

            return result;
        }

        public async Task<IEnumerable<BookBl>> GetAllAsync()
        {
            IEnumerable<BookBl> booksBl;

            using (var dbContext = new BookStoresDbContext())
            {
                booksBl = (await dbContext.Books
                    .Include(b => b.Author)
                    .ToListAsync())
                    .Select(book => _modelsMapper.MapBookFromDataToBusinessModel(book));
            }

            return booksBl;
        }

        public async Task<IEnumerable<BookBl>> SearchAsync(string namePart, string titlePart)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                return (await dbContext.Books
                    .Include(b => b.Author)
                    .ToListAsync())
                    .Where(book =>
                        $"{book.Author.Name} {book.Author.Surname}".ToLower().Contains(namePart.ToLower())
                        && book.Title.ToLower().Contains(titlePart.ToLower())
                    )
                    .Select(book => _modelsMapper.MapBookFromDataToBusinessModel(book));
            }
        }

        public async Task<IEnumerable<GenreBookPricesData>> GetBooksPriceDataByGenreAsync(int bookStoreId)
        {
            throw new NotImplementedException();
            //var result = new List<GenreBookPricesData>();
            //var allBooks = DataProvider.Books;
            //var availableGenres = allBooks.Select(book => book.BookGenre);

            //foreach (var genre in availableGenres)
            //{
            //    var minPrice = allBooks.Where(book => book.BookGenre == genre)
            //                           .Min(book => book.Price);
            //    var maxPrice = allBooks.Where(book => book.BookGenre == genre)
            //                           .Max(book => book.Price);
            //    var avgPrice = allBooks.Where(book => book.BookGenre == genre)
            //                           .Average(book => book.Price);

            //    var item = new GenreBookPricesData
            //    {
            //        Genre = _modelsMapper.MapBookGenreFromDataToBusiness(genre),
            //        MinPrice = minPrice,
            //        MaxPrice = maxPrice,
            //        AvgPrice = avgPrice
            //    };

            //    result.Add(item);
            //}

            //return result;
        }
    }

    public class BookAddedEventArgs
    {
        public BookBl ArrivedBook;
    }
}