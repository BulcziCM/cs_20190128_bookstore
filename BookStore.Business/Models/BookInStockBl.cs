﻿namespace BookStore.Business.Models
{
    public class BookInStockBl
    {
        public int Id;
        public int BookId;
        public int BookStoreId;
        public double Price;
        public int SoldCopies;
    }
}