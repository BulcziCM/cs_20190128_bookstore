﻿using System;

namespace BookStore.Business.Models
{
    public class AuthorBl
    {
        public int Id;
        public string Name;
        public string Surname;
        public DateTime DateOfBirth;
    }
}