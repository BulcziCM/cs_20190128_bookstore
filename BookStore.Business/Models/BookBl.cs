﻿namespace BookStore.Business.Models
{
    public class BookBl
    {
        public enum Genre
        {
            Comedy,
            Drama,
            Horror,
            Fantasy
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public AuthorBl Author { get; set; }
        public Genre BookGenre { get; set; }
    }
}