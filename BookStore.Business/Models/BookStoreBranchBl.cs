﻿using System.Collections.Generic;

namespace BookStore.Business.Models
{
    public class BookStoreBranchBl
    {
        public int Id;
        public string City;
        public string Name;
        public List<BookInStockBl> AvailableBooks;
    }
}