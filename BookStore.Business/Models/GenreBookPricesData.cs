﻿namespace BookStore.Business.Models
{
    public class GenreBookPricesData
    {
        public BookBl.Genre Genre;
        public double MinPrice;
        public double MaxPrice;
        public double AvgPrice;
    }
}