﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.Business.Helpers;
using BookStore.Business.Models;
using BookStore.Data;
using BookStore.Data.Models;

namespace BookStore.Business
{
    public class SubscriptionsService
    {
        private readonly ModelsMapper _modelsMapper;
        private readonly BooksService _booksService;

        public SubscriptionsService(BooksService booksService)
        {
            _modelsMapper = new ModelsMapper();
            _booksService = booksService;
            _booksService.BookAdded += NewBookArrived;
        }

        private void NewBookArrived(object sender, BookAddedEventArgs e)
        {
            var arrivedBookId = e.ArrivedBook.Id;

            List<EmailSubscription> affectedSubscriptions;

            using (var dbContext = new BookStoresDbContext())
            {
                affectedSubscriptions = dbContext.EmailSubscriptions
                    .Where(es => es.BookId == arrivedBookId)
                    .ToList();
            }

            affectedSubscriptions.ForEach(subscription => SendNotification(subscription, e.ArrivedBook));
        }

        private void SendNotification(EmailSubscription subscription, BookBl arrivedBook)
        {
            Console.WriteLine($"Sending notification that {arrivedBook.Title} book arrived to {subscription.Email}");
        }

        public void AddSubscription(EmailSubscriptionBl subscription)
        {
            var subscriptionToAdd = _modelsMapper.MapSubscriptionFromBusinessToData(subscription);

            using (var dbContext = new BookStoresDbContext())
            {
                dbContext.EmailSubscriptions.Add(subscriptionToAdd);
                dbContext.SaveChanges();
            }
        }
    }
}