﻿using BookStore.Business.Models;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Business.Tests
{
    [TestFixture]
    public class IncomeReportGeneratorTests
    {
        [Test]
        public void IncomePerBookStoreReport_ValidInput_ValidOutput()
        {
            //Arrange
            var empikStore = new BookStoreBranchBl { Id = 1, City = "Gdansk", Name = "Empik" };
            var domKsiazkiStore = new BookStoreBranchBl { Id = 2, City = "Sopot", Name = "Dom Ksiazki" };

            var bookStoresStockTestData = new Dictionary<BookStoreBranchBl, IEnumerable<BookInStockBl>>
            {
                {   //KeyValuePair
                    empikStore, //Key
                    new List<BookInStockBl> //Value
                    {
                        new BookInStockBl { BookId = 1, BookStoreId = 1, Price = 20, SoldCopies = 100 },
                        new BookInStockBl { BookId = 2, BookStoreId = 1, Price = 50, SoldCopies = 10  }
                    }
                },
                {   //KeyValuePair
                    domKsiazkiStore, //Key
                    new List<BookInStockBl> //Value
                    {
                        new BookInStockBl { BookId = 1, BookStoreId = 2, Price = 15, SoldCopies = 20 },
                        new BookInStockBl { BookId = 2, BookStoreId = 2, Price = 40, SoldCopies = 80 }
                    }
                }
            };

            var bookStoresServiceMock = new Mock<IBookStoresService>();
            bookStoresServiceMock.Setup(x => x.GetBookStoresStock()).Returns(bookStoresStockTestData);

            var incomeReportGenerator = new IncomeReportGenerator(null, bookStoresServiceMock.Object);

            //Act
            var result = incomeReportGenerator.GetIncomePerBookStore();

            //Assert
            Assert.AreEqual(2500, result[empikStore]);
            Assert.AreEqual(3500, result[domKsiazkiStore]);
        }

        [Test]
        public void IncomePerBookReport_ValidInput_ValidOutpu()
        {
            //Arrange
            var forumStore = new BookStoreBranchBl { Id = 1, City = "Gdansk", Name = "Forum" };
            var empikStore = new BookStoreBranchBl { Id = 2, City = "Sopot",  Name = "Empik" };

            var bookStoresStockTestData = new Dictionary<BookStoreBranchBl, IEnumerable<BookInStockBl>>
            {
                {
                    //KeyPairValue
                    forumStore,
                    new List<BookInStockBl> //value
                    {
                        new BookInStockBl {BookId = 1, BookStoreId = 1, Price = 20, SoldCopies = 100},
                        new BookInStockBl {BookId = 2, BookStoreId = 1, Price = 50, SoldCopies = 10}
                    }
                },
                {
                    //KeyPairValue
                    empikStore,
                    new List<BookInStockBl> //value
                    {
                        new BookInStockBl {BookId = 1, BookStoreId = 2, Price = 15, SoldCopies = 20},
                        new BookInStockBl {BookId = 2, BookStoreId = 2, Price = 40, SoldCopies = 80}
                    }
                }
            };

            var harryPotterBook = new BookBl() { Id = 1, Title = "Harry Potter" };
            var quoVadisBook    = new BookBl() { Id = 2, Title = "Quo vadis" };

            var bookStoreServiceMock = new Mock<IBookStoresService>();
            bookStoreServiceMock.Setup(x => x.GetBookStoresStock()).Returns(bookStoresStockTestData);

            var bookServiceMock = new Mock<IBooksService>();

            bookServiceMock.Setup(x => x.GetAsync(1)).Returns(Task.Factory.StartNew(() => harryPotterBook));
            bookServiceMock.Setup(x => x.GetAsync(2)).Returns(Task.Factory.StartNew(() => quoVadisBook));

            var incomeReportGenerator = new IncomeReportGenerator(bookServiceMock.Object, bookStoreServiceMock.Object);

            //Act
            var result = incomeReportGenerator.GetIncomePerBook();

            //Assert
            Assert.AreEqual(2300, result[harryPotterBook]);
            Assert.AreEqual(3700, result[quoVadisBook]);

            bookStoreServiceMock.Verify(x => x.GetBookStoresStock(), Times.Once);
        }
    }
}