﻿using System.Collections.Generic;

namespace BookStore.Data.Models
{
    public class Book
    {
        public enum Genre
        {
            Comedy,
            Drama,
            Horror,
            Fantasy
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Author Author { get; set; }
        public List<BookInStock> BookInStock { get; set; }
        public Genre BookGenre { get; set; }
    }
}