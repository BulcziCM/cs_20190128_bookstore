﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.Data.Models
{
    public class BookInStock
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int BookStoreBranchId { get; set; }
        public double Price { get; set; }
        public int SoldCopies { get; set; }
    }
}