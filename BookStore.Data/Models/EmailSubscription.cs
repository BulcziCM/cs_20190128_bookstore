﻿namespace BookStore.Data.Models
{
    public class  EmailSubscription
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public string Email { get; set; }
    }
}