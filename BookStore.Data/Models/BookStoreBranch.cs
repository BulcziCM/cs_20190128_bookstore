﻿using System.Collections.Generic;

namespace BookStore.Data.Models
{
    public class BookStoreBranch
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public List<BookInStock> AvailableBooks { get; set; }
    }
}