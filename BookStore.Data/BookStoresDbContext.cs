﻿using BookStore.Data.Models;
using System.Configuration;
using System.Data.Entity;

namespace BookStore.Data
{
    public class BookStoresDbContext : DbContext
    {
        public BookStoresDbContext() : base(GetConnectionString())
        { }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookStoreBranch> BookStores { get; set; }
        public DbSet<BookInStock> BooksInStock { get; set; }
        public DbSet<EmailSubscription> EmailSubscriptions { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["BookStoresConnectionString"].ConnectionString;
        }
    }
}